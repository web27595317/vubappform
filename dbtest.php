<?php
// Simulated database
$db = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // Get form data
  $lname = $_POST['lname'];
  $fname = $_POST['fname'];
  $fathername = $_POST['fathername'];
  $jmbg = $_POST['jmbg'];
  $oib = $_POST['oib'];
  $phone = $_POST['phone'];
  $mobile = $_POST['mobile'];
  $email = $_POST['email'];
  $gender = $_POST['gender'];
  $nationality = $_POST['nationality'];
  $ethnicity = $_POST['ethnicity'];
  $address = $_POST['address'];
  $school = $_POST['school'];

  // Handle file upload
  $file = $_FILES['file'];
  $fileName = $_FILES['file']['name'];
  $fileTmpName = $_FILES['file']['tmp_name'];
  $fileSize = $_FILES['file']['size'];
  $fileError = $_FILES['file']['error'];
  $fileType = $_FILES['file']['type'];

  $fileExt = explode('.', $fileName);
  $fileActualExt = strtolower(end($fileExt));

  $allowed = array('jpg', 'jpeg', 'png', 'txt', "");

  if (in_array($fileActualExt, $allowed)) {
    if ($fileError === 0) {
      if ($fileSize < 1000000) { // File size less than 1MB
        // $fileNameNew = uniqid('', true) . "." . $fileActualExt;
        $fileDestination = 'uploads/' . $fileName;
        move_uploaded_file($fileTmpName, $fileDestination);
        echo "File successfully uploaded!";
      } else {
        echo "Your file is too big!";
      }
    } else {
      if($fileExt == null){
        echo "There was an error uploading your file!";
      }
    }
  } else {
    echo "You cannot upload files of this type!";
  }

  // Simulate inserting data into the database
  $db[] = [
    'lname' => $lname,
    'fname' => $fname,
    'fathername' => $fathername,
    'jmbg' => $jmbg,
    'oib' => $oib,
    'phone' => $phone,
    'mobile' => $mobile,
    'email' => $email,
    'gender' => $gender,
    'nationality' => $nationality,
    'ethnicity' => $ethnicity,
    'address' => $address,
    'school' => $school,
  ];

}
?>

<!DOCTYPE html>
<html>
<link rel="stylesheet" href="styles.css">

<body>
  <form action="" method="post" enctype="multipart/form-data">
    <label for="lname">Prezime:</label><br>
    <input type="text" id="lname" name="lname"><br>
    <label for="fname">Ime:</label><br>
    <input type="text" id="fname" name="fname"><br>
    <label for="fathername">Ime oca:</label><br>
    <input type="text" id="fathername" name="fathername"><br>
    <label for="jmbg">JMBG:</label><br>
    <input type="text" id="jmbg" name="jmbg"><br>
    <label for="oib">OIB:</label><br>
    <input type="text" id="oib" name="oib"><br>
    <label for="phone">Broj telefona:</label><br>
    <input type="text" id="phone" name="phone"><br>
    <label for="mobile">Broj mobitela:</label><br>
    <input type="text" id="mobile" name="mobile"><br>
    <label for="email">E-mail adresa:</label><br>
    <input type="email" id="email" name="email"><br>
    <label for="gender">Spol:</label><br>
    <input type="radio" id="male" name="gender" value="M">
    <label for="male">M</label>
    <input type="radio" id="female" name="gender" value="Z">
    <label for="female">Ž</label><br>
    <label for="nationality">Državljanstvo:</label><br>
    <input type="text" id="nationality" name="nationality"><br>
    <label for="ethnicity">Narodnost:</label><br>
    <input type="text" id="ethnicity" name="ethnicity"><br>
    <label for="address">Adresa prebivališta:</label><br>
    <input type="text" id="address" name="address"><br>
    <label for="school">Puni naziv završene srednje škole:</label><br>
    <input type="text" id="school" name="school"><br>
    <label for="file">Upload a file:</label><br>
    <input type="file" id="file" name="file"><br>
    <input type="submit" value="Submit">
  </form>

</body>

</html>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  echo "<pre>";
  echo "Data successfully inserted!";
  print_r($db);
  echo "<pre>";
}
?>