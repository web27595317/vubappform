<?php
$host = 'localhost';
$port = 5432;
$dbname = 'postgres';
$user = 'postgres';
$password = '123';
$dsn = "pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password";

try {
  $pdo = new PDO($dsn);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $lname = $_POST['lname'];
  $fname = $_POST['fname'];
  $fathername = $_POST['fathername'];
  $jmbg = $_POST['jmbg'];
  $oib = $_POST['oib'];
  $phone = $_POST['phone'];
  $mobile = $_POST['mobile'];
  $email = $_POST['email'];
  $gender = $_POST['gender'];
  $nationality = $_POST['nationality'];
  $ethnicity = $_POST['ethnicity'];
  $address = $_POST['address'];
  $school = $_POST['school'];

  // Prepare SQL query
  $query = "INSERT INTO your_table_name (lname, fname, fathername, jmbg, oib, phone, mobile, email, gender, nationality, ethnicity, address, school) VALUES (:lname, :fname, :fathername, :jmbg, :oib, :phone, :mobile, :email, :gender, :nationality, :ethnicity, :address, :school)";
  $stmt = $pdo->prepare($query);

  // Bind parameters
  $stmt->bindParam(':lname', $lname);
  $stmt->bindParam(':fname', $fname);
  $stmt->bindParam(':fathername', $fathername);
  $stmt->bindParam(':jmbg', $jmbg);
  $stmt->bindParam(':oib', $oib);
  $stmt->bindParam(':phone', $phone);
  $stmt->bindParam(':mobile', $mobile);
  $stmt->bindParam(':email', $email);
  $stmt->bindParam(':gender', $gender);
  $stmt->bindParam(':nationality', $nationality);
  $stmt->bindParam(':ethnicity', $ethnicity);
  $stmt->bindParam(':address', $address);
  $stmt->bindParam(':school', $school);

  // Execute SQL query
  if ($stmt->execute()) {
    echo "Data successfully inserted!";
  } else {
    echo "An error occurred.";
  }
}

?>

<!DOCTYPE html>
<html>

<body>
  <form action="" method="post">
    <label for="lname">Prezime:</label><br>
    <input type="text" id="lname" name="lname"><br>
    <label for="fname">Ime:</label><br>
    <input type="text" id="fname" name="fname"><br>
    <label for="fathername">Ime oca:</label><br>
    <input type="text" id="fathername" name="fathername"><br>
    <label for="jmbg">JMBG:</label><br>
    <input type="text" id="jmbg" name="jmbg"><br>
    <label for="oib">OIB:</label><br>
    <input type="text" id="oib" name="oib"><br>
    <label for="phone">Broj telefona:</label><br>
    <input type="text" id="phone" name="phone"><br>
    <label for="mobile">Broj mobitela:</label><br>
    <input type="text" id="mobile" name="mobile"><br>
    <label for="email">E-mail adresa:</label><br>
    <input type="email" id="email" name="email"><br>
    <label for="gender">Spol:</label><br>
    <input type="radio" id="male" name="gender" value="M">
    <label for="male">M</label><br>
    <input type="radio" id="female" name="gender" value="Z">
    <label for="female">Ž</label><br>
    <label for="nationality">Državljanstvo:</label><br>
    <input type="text" id="nationality" name="nationality"><br>
    <label for="ethnicity">Narodnost:</label><br>
    <input type="text" id="ethnicity" name="ethnicity"><br>
    <label for="address">Adresa prebivališta:</label><br>
    <input type="text" id="address" name="address"><br>
    <label for="school">Puni naziv završene srednje škole:</label><br>
    <input type="text" id="school" name="school"><br>
    <input type="submit" value="Submit">
  </form>

</body>

</html>